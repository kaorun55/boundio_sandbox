﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace geis
{
    public class boundio
    {
        string serial;
        string key;

        // URLのベース
        const string urlBase = "https://boundio.jp/api/vd1/";
        //const string urlBase = "https://boundio.jp/api/v1/";

        // エラーメッセージ
        static Dictionary<int, string> error = new Dictionary<int, string>()
        {
            { 1, "ユーザーシリアルID、もしくはAPIアクセスキーの認証に失敗" },
	        { 2, "電話番号の書式エラー " },
	        { 3, "音声ファイルなし" },
	        { 4, "ポイント不足" },
	        { 5, "アプリケーションの電話番号エラー" },
	        { 99, "システムエラー・その他不明なエラー" },
        };

        public boundio( string serial, string key )
        {
            this.serial = serial;
            this.key = key;
        }

        // 架電する
        public int Call( string tel_to, string cast )
        {
            string url = urlBase + serial + "/call";
            string param = string.Format( "key={0}&tel_to={1}&cast={2}", key, tel_to, cast );
            byte[] data = boundio.Encoding.GetBytes( param );

            // リクエストの作成  
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create( url );
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";
            req.ContentLength = data.Length;

            // ポスト・データの送信  
            using ( Stream reqStream = req.GetRequestStream() ) {
                reqStream.Write( data, 0, data.Length );
            }

            // レスポンスの取得  
            using ( WebResponse res = req.GetResponse() ) {
                using ( Stream resStream = res.GetResponseStream() ) {
                    var ret = JsonDecode( resStream );
                    if ( ret.success ) {
                        return ret._id;
                    }
                    else {
                        throw new Exception( error[ret.error] );
                    }
                }
            }
        }

        // エンコード
        public static Encoding Encoding
        {
            get{ return Encoding.UTF8; }
        }

        // JSONをデコードする
        #region CallResponse
        [DataContract]
        public class CallResponse
        {
            [DataMember]
            public bool success
            {
                get;
                set;
            }

            [DataMember]
            public int _id
            {
                get;
                set;
            }

            [DataMember]
            public int error
            {
                get;
                set;
            }
        }
        #endregion

        public static CallResponse JsonDecode( Stream sr )
        {
            DataContractJsonSerializer json= new DataContractJsonSerializer( typeof( CallResponse ) );
            return json.ReadObject( sr ) as CallResponse;
        }
    }
}
