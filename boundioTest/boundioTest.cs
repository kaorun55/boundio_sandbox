﻿using System;
using System.IO;
using geis;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace boundioTest
{
    /// <summary>
    /// UnitTest1 の概要の説明
    /// </summary>
    [TestClass]
    public class boundioTest
    {
        public boundioTest()
        {
            //
            // TODO: コンストラクター ロジックをここに追加します
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///現在のテストの実行についての情報および機能を
        ///提供するテスト コンテキストを取得または設定します。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 追加のテスト属性
        //
        // テストを作成する際には、次の追加属性を使用できます:
        //
        // クラス内で最初のテストを実行する前に、ClassInitialize を使用してコードを実行してください
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // クラス内のテストをすべて実行したら、ClassCleanup を使用してコードを実行してください
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 各テストを実行する前に、TestInitialize を使用してコードを実行してください
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 各テストを実行した後に、TestCleanup を使用してコードを実行してください
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void JsonDecodeTest_Success()
        {
            string json = "{\"success\":\"true\",\"_id\":\"2319\"}";

            var res = boundio.JsonDecode( new MemoryStream( boundio.Encoding.GetBytes( json ) ) );

            Assert.IsTrue( res.success );
            Assert.AreEqual( res._id, 2319 );
            Assert.AreEqual( res.error, 0 );
        }

        [TestMethod]
        public void JsonDecodeTest_Fail()
        {
            string json = "{\"success\":\"false\",\"error\":\"1\"}";

            var res = boundio.JsonDecode( new MemoryStream( boundio.Encoding.GetBytes( json ) ) );

            Assert.IsFalse( res.success );
            Assert.AreEqual( res._id, 0 );
            Assert.AreEqual( res.error, 1 );
        }

        string serial = "URNJ8NF9HM25KZH9";
        //            string serial = "URNJ8NF9HM25KZH";
        string key = "eSxAwZ1qzHY5Bpy6";


        [TestMethod, Ignore]
        public void Call()
        {
            string tel_to = "09080246375";
            string cast = "file(000002)";

            boundio target = new boundio( serial, key );
            target.Call( tel_to, cast );
        }

        [TestMethod]
        public void Call_RandNum()
        {
            string tel_to = "09080246375";
            string cast = "file(000002)";

            Random rnd = new Random( Environment.TickCount );
            int[] otp = new int[] {
                rnd.Next( 0, 9 ),
                rnd.Next( 0, 9 ),
                rnd.Next( 0, 9 ),
                rnd.Next( 0, 9 ),
            };

            foreach ( var val in otp ) {
                cast += "%%num(" + val + ")";
            }

            boundio target = new boundio( serial, key );
            target.Call( tel_to, cast );
        }
    }
}
